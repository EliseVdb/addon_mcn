import bpy

class MCN_PT_EV(bpy.types.Panel):
    """"""
    bl_label = "EV"
    bl_idname = "MCN_PT_EV"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout

        # TODO Replace this label by your stuff here
        layout.label(text="display comprehension")

    def execute
    comprehension = bpy.ops.object.empty_add(type='PLAIN_AXES', radius=1.0, align='WORLD');

def register():
    bpy.utils.register_class(MCN_PT_EV)

def unregister():
    bpy.utils.unregister_class(MCN_PT_EV)